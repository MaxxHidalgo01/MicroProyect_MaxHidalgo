using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HUD : MonoBehaviour
{
    public static HUD Instance;
    public TextMeshProUGUI puntos;
    public GameObject [] vidas;

    private void Awake(){
        if (HUD.Instance == null){
            HUD.Instance = this;
            DontDestroyOnLoad(this);
        } 
        else{
            Destroy(gameObject);
        }
    }
    void Update() {
        puntos.text = GameManager.Instance.PuntosTotales.ToString();
    }

     public void ActualizarPuntos(int puntosTotales)
     {
         puntos.text = puntosTotales.ToString();
     }

    public void DesactivarVida(int indice) {
        vidas[indice].SetActive(false);
    }

    public void ActivarVida(int indice) {
        vidas[indice].SetActive(true);
    }
    public void destruirHud(){
        Destroy(gameObject);
    }
}
 