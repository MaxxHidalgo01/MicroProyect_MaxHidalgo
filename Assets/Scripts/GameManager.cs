using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

  

   //public int PuntosTotales {get; private set; }
    public int PuntosTotales { get{ return puntosTotales;}}
    private int puntosTotales = 0;

    private int scene = 1;
    
   private int vidas = 3;

   private bool NivelUno = false;
   private bool NivelDos = true;

   private void Awake ()
    {
     if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
           Destroy(gameObject);
            Debug.Log ("Cuidado! Mas de un GameManager en escena. ");
        }
    }

    public void SumarPuntos (int puntoASumar)
    {
        puntosTotales += puntoASumar;
        //hud.ActualizarPuntos(PuntosTotales);
        if(puntosTotales >= 4 && NivelUno == false){
            NivelUno = true;
            NivelDos = false;
            puntosTotales = 0;
            FinNivel();
        }
        else if (puntosTotales >= 3 && NivelDos == false){
            NivelDos = true;
            puntosTotales = 0;
            FinNivel();
        }
        else if (puntosTotales >= 5){
            NivelUno = false;
            puntosTotales = 0;
            FinNivel(); 
            scene = 1;
        }
    }

    public void PerderVida() {
        vidas -= 1;

        if (vidas <= 0)
        {
            // El nivel se Reinicia.
            ReiniciarNivel();
            return;
        }
        // else{
        //     HUD.Instance.DesactivarVida(vidas);
        // }
        HUD.Instance.DesactivarVida(vidas);
    }

    public bool RecuperarVida() {
        if (vidas >= 3)
        {
            return false;
        }
        HUD.Instance.ActivarVida(vidas);
         vidas += 1;
         return true; 
    }
    private void FinNivel(){
        scene ++;
        ReiniciarNivel();
    }
    public void ReiniciarNivel(){
        puntosTotales = 0;
        for (int i = 0; i < 3; i++){
            RecuperarVida();
        }
        if (scene == 4){
            SceneManager.LoadScene(scene);
            scene = 0;
            
            HUD.Instance.destruirHud();
            Destroy(gameObject);
            return;
        }
        SceneManager.LoadScene(scene);
    }
}
