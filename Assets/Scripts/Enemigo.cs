using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    public int vida = 2;
    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.CompareTag("Player")) {
            GameManager.Instance.PerderVida();
        }
    }
    
    public void tomarDaño(int damage){
        vida = vida - damage;
        if(vida <= 0) {
            Destroy(gameObject);
        }
    }
}
